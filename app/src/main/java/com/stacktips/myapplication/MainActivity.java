package com.stacktips.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private static final int MAX_LENGTH = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initializing EditText
        EditText editText = (EditText) findViewById(R.id.editText1);

        //Set Length filter. Restricting to 10 characters only
        //editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH)});

        //Allowing only upper case characters
        //editText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        //Setting two or more filters
        //editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH), new InputFilter.AllCaps()});

        //Define Min, Max range value using custom input filter
        editText.setFilters(new InputFilter[]{new CustomRangeInputFilter(0f, 10.0f)});
    }
}
